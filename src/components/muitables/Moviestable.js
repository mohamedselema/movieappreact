import React, { Component } from 'react';
import MUIDataTable from "mui-datatables";
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import RateMovie from './Rating/RateMovie';
//import DeleteRoundedIcon from '@material-ui/icons/DeleteOutlined';

const styles = theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  formControl: {
    maxWidth: 80,
  },
  icon: {
    margin: theme.spacing.unit,
    fontSize: 32,
  },
});

const movies = [
  {
    _id: "5b21ca3eeb7f6fbccd471815",
    title: "Terminator",
    genre: { _id: "5b21ca3eeb7f6fbccd471818", name: "Action" },
    numberInStock: 6,
    dailyRentalRate: 2.5,
    publishDate: "2018-01-03T19:04:28.809Z"
  },
  {
    _id: "5b21ca3eeb7f6fbccd471816",
    title: "Die Hard",
    genre: { _id: "5b21ca3eeb7f6fbccd471818", name: "Action" },
    numberInStock: 5,
    dailyRentalRate: 2.5
  },
  {
    _id: "5b21ca3eeb7f6fbccd471817",
    title: "Get Out",
    genre: { _id: "5b21ca3eeb7f6fbccd471820", name: "Thriller" },
    numberInStock: 8,
    dailyRentalRate: 3.5
  },
  {
    _id: "5b21ca3eeb7f6fbccd471819",
    title: "Trip to Italy",
    genre: { _id: "5b21ca3eeb7f6fbccd471814", name: "Comedy" },
    numberInStock: 7,
    dailyRentalRate: 3.5
  },
  {
    _id: "5b21ca3eeb7f6fbccd47181a",
    title: "Airplane",
    genre: { _id: "5b21ca3eeb7f6fbccd471814", name: "Comedy" },
    numberInStock: 7,
    dailyRentalRate: 3.5
  },
  {
    _id: "5b21ca3eeb7f6fbccd47181b",
    title: "Wedding Crashers",
    genre: { _id: "5b21ca3eeb7f6fbccd471814", name: "Comedy" },
    numberInStock: 7,
    dailyRentalRate: 3.5
  },
  {
    _id: "5b21ca3eeb7f6fbccd47181e",
    title: "Gone Girl",
    genre: { _id: "5b21ca3eeb7f6fbccd471820", name: "Thriller" },
    numberInStock: 7,
    dailyRentalRate: 4.5
  },
  {
    _id: "5b21ca3eeb7f6fbccd47181f",
    title: "The Sixth Sense",
    genre: { _id: "5b21ca3eeb7f6fbccd471820", name: "Thriller" },
    numberInStock: 4,
    dailyRentalRate: 3.5
  },
  {
    _id: "5b21ca3eeb7f6fbccd471821",
    title: "The Avengers",
    genre: { _id: "5b21ca3eeb7f6fbccd471818", name: "Action" },
    numberInStock: 7,
    dailyRentalRate: 3.5
  }
];

class Moivetable extends Component {
  state = {
    stateMovies: movies,
    favMoives: []
  }
  AddtoFav(id) {
    alert(id); this.setState({
      favMoives: [...this.state.favMoives, id]
    })
    //console.log(id);
  }
  RemoveFromFav(id) {
    alert(id);
    var favMoives = [...this.state.favMoives];
    var index = favMoives.indexOf(id);
    favMoives.splice(index, 1);
    this.setState({ favMoives: favMoives });
  }

  render() {
    const { classes } = this.props;
    let twoDArray = [];
    if (!this.state.error) {
      this.state.stateMovies.map((movie, index) => {
        twoDArray[index] = [];
        twoDArray[index].push(movie.title);
        twoDArray[index].push(movie.genre.name);
        twoDArray[index].push(movie.numberInStock);
        twoDArray[index].push(movie.dailyRentalRate);
        twoDArray[index].push(movie._id);
      });
    }
    console.log(twoDArray);

    const columns = [
      "Title",
      "Genre",
      "Stock",
      {
        name: "Rating",
        options: {
          filter: true,
          customRender: (value, tableMeta, updateValue) => {
            return (
              <div>
                <RateMovie ratevalue={value} />
              </div>
            );
          }
        }
      },
      {
        name: "Fav",
        options: {
          filter: true,
          customRender: (value, tableMeta, updateValue) => {
            if (this.state.favMoives.includes(value)) {
              return (
                <div>
                  <i
                    onClick={() => this.RemoveFromFav(value)}
                    class="material-icons">favorite</i>
                </div>);
            } else {
              return (<div>
                <i onClick={() => this.AddtoFav(value)}
                  class="material-icons">favorite_border </i>
              </div>);
            }
          }
        }
      },
    ];
    const options = {
      filterType: "dropdown",
      responsive: "stacked"
    };
    return (
      <div>
        <MUIDataTable
          title={"Moive App"}
          data={twoDArray}
          columns={columns}
          options={options}
        />
      </div>
    );
  }
}

Moivetable.propTypes = {
  classes: PropTypes.object.isRequired,
};
export default withStyles(styles)(Moivetable);