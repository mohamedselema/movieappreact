import React, { Component } from 'react';
import { Rating } from 'material-ui-rating';
class RateMovie extends Component {
  render() {
    return (
      <div>
        <Rating
          value={this.props.ratevalue}
          max={5}
          onChange={(value) => console.log(`Rated with value ${value}`)}
        />
      </div>
    );
  }
}

export default RateMovie;